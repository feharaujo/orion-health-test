package com.fearaujo.orionhealthtest.home;

import com.fearaujo.data.injection.ApplicationComponent;
import com.fearaujo.orionhealthtest.home.contact.ContactsFragment;

import dagger.Component;

/**
 * Created by felipearaujo on 06/01/17.
 *
 * Dagger component for Home flow, injects all the dependencies.
 */
@HomeScope
@Component(modules = HomeModule.class, dependencies = ApplicationComponent.class)
public interface HomeComponent {

    void inject(HomeActivity target);

    void inject(ContactsFragment target);

}
