package com.fearaujo.orionhealthtest.home;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by felipearaujo on 07/01/17.
 */

public interface IBasePresenter<T> {

    void setView(T view);

    void onViewCreated(@Nullable Bundle savedInstanceState);


}
