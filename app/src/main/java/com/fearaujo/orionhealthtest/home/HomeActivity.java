package com.fearaujo.orionhealthtest.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.fearaujo.data.model.User;
import com.fearaujo.orionhealthtest.R;
import com.fearaujo.orionhealthtest.application.AppApplication;
import com.fearaujo.orionhealthtest.home.contact.ContactsFragment;
import com.fearaujo.orionhealthtest.home.detail.DetailsActivity;
import com.fearaujo.orionhealthtest.home.detail.DetailsFragment;

public class HomeActivity extends AppCompatActivity implements IHome {

    private DetailsFragment mDetailsFragment;
    private ContactsFragment mContactsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppApplication application = (AppApplication) getApplication();
        HomeComponent component = application.getHomeComponent();

        if (component == null) {
            component = DaggerHomeComponent.builder()
                    .applicationComponent(((AppApplication) getApplication()).getApplicationComponent())
                    .build();
            ((AppApplication) getApplication()).setHomeComponent(component);
        }

        setContentView(R.layout.activity_main);

        mContactsFragment = (ContactsFragment) getSupportFragmentManager().findFragmentById(R.id.frag_contacts);
        mDetailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.frag_details);

        checkCellNeedSavePosition();
    }

    /**
     * Check if the screen has the {@link DetailsFragment}.
     * On tablets when selecting an {@link User} the cell must remain marked.
     */
    private void checkCellNeedSavePosition() {
        boolean saveClickPosition = false;

        if (mDetailsFragment != null) {
            saveClickPosition = true;
        }

        mContactsFragment.setSaveClickPosition(saveClickPosition);
    }

    /**
     * Update the {@link DetailsFragment} with the user selected.
     *
     * @param user User selected
     */
    @Override
    public void updateDetailsFragment(User user) {
        if (mDetailsFragment != null) {
            mDetailsFragment.updateInfos(user);
        } else {
            Intent intent = new Intent(HomeActivity.this, DetailsActivity.class);
            intent.putExtra(User.USER_EXTRA, user);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_sort_a_z) {
            mContactsFragment.sortRecyclerViewAZ();
            return true;
        } else if (id == R.id.menu_sort_z_a) {
            mContactsFragment.sortRecyclerViewZA();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
