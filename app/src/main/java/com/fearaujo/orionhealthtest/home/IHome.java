package com.fearaujo.orionhealthtest.home;

import com.fearaujo.data.model.User;

/**
 * Created by felipearaujo on 08/01/17.
 *
 */
public interface IHome {

    void updateDetailsFragment(User user);

}
