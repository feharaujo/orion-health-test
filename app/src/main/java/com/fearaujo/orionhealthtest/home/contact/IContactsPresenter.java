package com.fearaujo.orionhealthtest.home.contact;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fearaujo.data.model.User;
import com.fearaujo.orionhealthtest.home.IBasePresenter;

import java.util.List;

/**
 * Created by felipearaujo on 07/01/17.
 */

public interface IContactsPresenter extends IBasePresenter<IContacts> {

    Bundle onSaveInstanceState(Bundle outState, @Nullable Integer selectedPosition, @Nullable List<User> users);

    void onViewStateRestored(@Nullable Bundle savedInstanceState);


}
