package com.fearaujo.orionhealthtest.home.detail;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.fearaujo.data.model.User;
import com.fearaujo.orionhealthtest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.frameDetails)
    FrameLayout frameDetails;

    private DetailsFragment mDetailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        setupView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUserExtra();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Set the {@link DetailsFragment} in the view
     */
    private void setupView() {
        mDetailsFragment = new DetailsFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(frameDetails.getId(), mDetailsFragment);
        transaction.commit();
    }

    /**
     * Set the {@link User} data and title
     */
    private void setUserExtra() {
        User user = getIntent().getParcelableExtra(User.USER_EXTRA);
        setTitle(user.getName());
        mDetailsFragment.updateInfos(user);
    }


}
