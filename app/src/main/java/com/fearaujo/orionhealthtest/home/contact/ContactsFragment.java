package com.fearaujo.orionhealthtest.home.contact;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fearaujo.data.model.User;
import com.fearaujo.orionhealthtest.R;
import com.fearaujo.orionhealthtest.application.AppApplication;
import com.fearaujo.orionhealthtest.home.HomeActivity;
import com.fearaujo.orionhealthtest.home.HomeComponent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment implements IContacts, ContactsAdapter.AdapterOnClickHandler {

    @BindView(R.id.rv_contacts)
    RecyclerView rvContacts;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.tvErrorMessage)
    TextView tvErrorMessage;

    @Inject
    ContactsPresenter presenter;
    @Inject
    ContactsAdapter adapter;

    private HomeActivity mActivity;

    private Unbinder mUnbinder;

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        HomeComponent component = ((AppApplication) getActivity().getApplication()).getHomeComponent();
        component.inject(this);

        presenter.setView(this);

        setupRecyclerView();

        presenter.onViewCreated(savedInstanceState);
    }

    /**
     * Enable the recycler view cell to be marked when selected (Just in tablets)
     *
     * @param saveClickPosition Cell can/cannot be selected
     */
    public void setSaveClickPosition(boolean saveClickPosition) {
        adapter.setSaveClickPosition(saveClickPosition);
    }

    /**
     * Setup the recycler view
     */
    private void setupRecyclerView() {
        adapter.setClickHandler(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvContacts.setLayoutManager(layoutManager);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvContacts.getContext(),
                layoutManager.getOrientation());
        rvContacts.addItemDecoration(mDividerItemDecoration);
        rvContacts.setHasFixedSize(true);
        rvContacts.setAdapter(adapter);
    }

    /**
     * Menu action for sort the users
     */
    public void sortRecyclerViewAZ() {
        adapter.sortRecyclerViewAZ();
        notifyChangesRecyclerView();
    }

    /**
     * Menu action for sort the users
     */
    public void sortRecyclerViewZA() {
        adapter.sortRecyclerViewZA();
        notifyChangesRecyclerView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    /**
     * Get the activity instance
     *
     * @param context Activity context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof HomeActivity)
            mActivity = (HomeActivity) context;
    }

    /**
     * Retry get the users
     */
    @OnClick(R.id.tvErrorMessage)
    void errorMessageRetry() {
        presenter.getUsers();
    }

    /**
     * Update the recycler view with new users.
     *
     * @param users User list
     */
    @Override
    public void updateRecyclerView(List<User> users) {
        adapter.updateData(users);
    }

    /**
     * Set the error message visible
     */
    @Override
    public void showErrorMessage() {
        tvErrorMessage.setVisibility(View.VISIBLE);
    }

    /**
     * Hide the error message
     */
    @Override
    public void hideErrorMessage() {
        tvErrorMessage.setVisibility(View.GONE);
    }

    /**
     * Hide the progress bar
     */
    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * Show the progress bar
     */
    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Show the recycler view
     */
    @Override
    public void showRecyclerView() {
        rvContacts.setVisibility(View.VISIBLE);
    }

    /**
     * Hide the recycler view
     */
    @Override
    public void hideRecyclerView() {
        rvContacts.setVisibility(View.GONE);
    }

    /**
     * Set the user selected cell in the recycler view.
     * Used when the device changes the orientation or go to background.
     *
     * @param position User selected
     */
    @Override
    public void setSelectedPositionRecyclerView(@Nullable Integer position) {
        adapter.setSelectedPosition(position);
    }

    /**
     * Update the recycler view with the new data
     */
    @Override
    public void notifyChangesRecyclerView() {
        adapter.notifyDataSetChanged();
    }

    /**
     * Update the {@link com.fearaujo.orionhealthtest.home.detail.DetailsFragment} with the user data
     *
     * @param position User selected position
     * @param user     User selected
     */
    @Override
    public void click(int position, User user) {
        mActivity.updateDetailsFragment(user);
    }

    /**
     * Save the data to change orientation or go to background
     *
     * @param outState Bundle to save the data
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState = presenter.onSaveInstanceState(
                outState, adapter.getSelectedPosition(), adapter.getUsersList()
        );

        super.onSaveInstanceState(outState);
    }

    /**
     * Restore the data to the UI
     *
     * @param savedInstanceState Bundle with the data
     */
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        presenter.onViewStateRestored(savedInstanceState);
    }

}
