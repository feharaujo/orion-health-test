package com.fearaujo.orionhealthtest.home;

import com.fearaujo.data.service.IUserAPI;
import com.fearaujo.data.service.UserDataSource;
import com.fearaujo.orionhealthtest.home.contact.ContactsAdapter;
import com.fearaujo.orionhealthtest.home.contact.ContactsPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by felipearaujo on 06/01/17.
 * <p>
 * Dagger module for Home flow.
 */
@Module
public class HomeModule {

    /**
     * Provide the recycler view adapter
     *
     * @return Recycler view adapter
     */
    @HomeScope
    @Provides
    public ContactsAdapter providesAdapter() {
        return new ContactsAdapter();
    }

    /**
     * Provides the presenter for {@link com.fearaujo.orionhealthtest.home.contact.ContactsFragment}
     *
     * @param userService Data Source
     * @return Presenter
     */
    @HomeScope
    @Provides
    public ContactsPresenter providesContactsPresenter(UserDataSource userService) {
        return new ContactsPresenter(userService);
    }

    /**
     * Provides the data source for {@link ContactsPresenter}
     *
     * @param userAPI User API
     * @return Data source
     */
    @HomeScope
    @Provides
    public UserDataSource providesUserService(IUserAPI userAPI) {
        return new UserDataSource(userAPI);
    }

}
