package com.fearaujo.orionhealthtest.home.contact;

import android.support.annotation.Nullable;

import com.fearaujo.data.model.User;

import java.util.List;

/**
 * Created by felipearaujo on 07/01/17.
 */

public interface IContacts {

    void updateRecyclerView(List<User> users);

    void showErrorMessage();

    void hideErrorMessage();

    void hideProgressBar();

    void showProgressBar();

    void showRecyclerView();

    void hideRecyclerView();

    void setSelectedPositionRecyclerView(@Nullable Integer position);

    void notifyChangesRecyclerView();

}
