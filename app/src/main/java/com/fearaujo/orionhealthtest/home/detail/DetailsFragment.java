package com.fearaujo.orionhealthtest.home.detail;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fearaujo.data.model.User;
import com.fearaujo.orionhealthtest.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {

    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvCompany)
    TextView tvCompany;
    @BindView(R.id.rootLayout)
    ConstraintLayout rootLayout;

    private Unbinder mUnbinder;

    private User mLastUser;

    public static final String USER_KEY = "userKey";


    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    /**
     * Update the view with the user data.
     *
     * @param user User selected
     */
    public void updateInfos(@NonNull User user) {
        this.mLastUser = user;
        rootLayout.setVisibility(View.VISIBLE);

        tvAddress.setText(user.getAddress().toString());
        tvPhone.setText(user.getPhone());
        tvUsername.setText(user.getUsername());
        tvCompany.setText(user.getCompany().toString());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(USER_KEY, mLastUser);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            User user = savedInstanceState.getParcelable(USER_KEY);
            if (user != null) {
                updateInfos(user);
            }
        }
    }

}
