package com.fearaujo.orionhealthtest.home;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by felipearaujo on 06/01/17.
 *
 * Dagger scope for Home flow.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface HomeScope {
}
