package com.fearaujo.orionhealthtest.home.contact;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fearaujo.data.model.User;
import com.fearaujo.data.service.UserDataSource;
import com.fearaujo.data.util.SortUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by felipearaujo on 07/01/17.
 * <p>
 * Presenter for the {@link ContactsFragment}
 */
public class ContactsPresenter implements IContactsPresenter, Callback<List<User>> {

    /**
     * User data source
     */
    private UserDataSource mUserDataSource;

    /**
     * View
     */
    private IContacts mView;

    /**
     * Key for save the user position
     */
    private final String POSITION = "position";

    /**
     * Key for save the users list
     */
    private final String USERS_STATE = "usersState";

    @Inject
    public ContactsPresenter(UserDataSource userDataSource) {
        this.mUserDataSource = userDataSource;
    }

    @Override
    public void setView(IContacts view) {
        this.mView = view;
    }

    /**
     * Check if exists users saved in the Bundle and load them,
     * or start a Retrofit request for load the users.
     *
     * @param savedInstanceState Data saved when the device changes the orientation or enter in background
     */
    @Override
    public void onViewCreated(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            getUsers();
        } else {
            List<User> users = savedInstanceState.getParcelableArrayList(USERS_STATE);
            if (users != null) {
                updateInfos(users);
            } else {
                getUsers();
            }
        }
    }

    /**
     * Save data to change the orientation or enter in background
     *
     * @param outState         Bundle to save data
     * @param selectedPosition User position
     * @param users            List of users
     * @return Data saved when the device changes the orientation or enter in background
     */
    @Override
    public Bundle onSaveInstanceState(Bundle outState, @Nullable Integer selectedPosition, @Nullable List<User> users) {
        if (selectedPosition != null)
            outState.putInt(POSITION, selectedPosition);

        if (users != null) {
            ArrayList<User> usersArray = new ArrayList<>(users);
            outState.putParcelableArrayList(USERS_STATE, usersArray);
        }

        return outState;
    }

    /**
     * Set the User selected in recycler view
     *
     * @param savedInstanceState Data saved when the device changes the orientation or enter in background
     */
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            Integer lastPositionClicked = savedInstanceState.getInt(POSITION, -1);

            if (lastPositionClicked == -1) {
                lastPositionClicked = null;
            }

            mView.setSelectedPositionRecyclerView(lastPositionClicked);
            mView.notifyChangesRecyclerView();
        }
    }


    /**
     * Show the progress bar and search for the users
     */
    void getUsers() {
        mView.hideRecyclerView();
        mView.hideErrorMessage();
        mView.showProgressBar();

        this.mUserDataSource.searchUsers(this);
    }

    /**
     * Retrofit response success
     *
     * @param call     Response callback
     * @param response Response object
     */
    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
        if (response != null && response.body() != null) {
            List<User> users = SortUtil.sortAZ(response.body());
            updateInfos(users);
        } else {
            requestfail();
        }
    }

    /**
     * Retrofit response fail
     *
     * @param call Response callback
     * @param t    Exception
     */
    @Override
    public void onFailure(Call<List<User>> call, Throwable t) {
        requestfail();
    }

    /**
     * Update the recycler view with a user list
     *
     * @param users User list
     */
    private void updateInfos(@NonNull List<User> users) {
        mView.updateRecyclerView(users);
        mView.hideProgressBar();
        mView.hideErrorMessage();
        mView.showRecyclerView();
    }

    /**
     * Show a error message
     */
    private void requestfail() {
        mView.showErrorMessage();
        mView.hideRecyclerView();
        mView.hideProgressBar();
    }


}
