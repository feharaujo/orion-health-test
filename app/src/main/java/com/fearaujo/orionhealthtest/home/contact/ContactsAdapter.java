package com.fearaujo.orionhealthtest.home.contact;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fearaujo.data.model.User;
import com.fearaujo.data.util.SortUtil;
import com.fearaujo.orionhealthtest.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by felipearaujo on 07/01/17.
 * <p>
 * Adapter of the recycler view of Users
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder> {

    private List<User> mUsersList = new ArrayList<>();

    private AdapterOnClickHandler mClickHandler;
    private Integer mSelectedPosition;
    private boolean mSaveClickPosition;

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        int layoutIdForListItem = R.layout.item_user;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        holder.tvName.setText(mUsersList.get(position).getName());
        holder.tvEmail.setText(mUsersList.get(position).getEmail());

        if (mSaveClickPosition) {
            if (mSelectedPosition != null && mSelectedPosition == position) {
                // Here I am just highlighting the background
                holder.itemView.setBackgroundColor(Color.LTGRAY);
            } else {
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mUsersList.size();
    }

    /**
     * Update the data in the recycler view
     *
     * @param updatedUsers New users data
     */
    void updateData(List<User> updatedUsers) {
        mUsersList.clear();
        if (mUsersList != null) {
            mUsersList.addAll(updatedUsers);
        }

        notifyDataSetChanged();
    }

    void setClickHandler(AdapterOnClickHandler mClickHandler) {
        this.mClickHandler = mClickHandler;
    }

    void setSaveClickPosition(boolean saveClickPosition) {
        this.mSaveClickPosition = saveClickPosition;
    }

    void setSelectedPosition(@Nullable Integer selectedPosition) {
        this.mSelectedPosition = selectedPosition;
    }

    Integer getSelectedPosition() {
        return mSelectedPosition;
    }

    /**
     * Sort list alphabetically (A-Z) and get the new user position.
     */
    void sortRecyclerViewAZ() {
        User currentSelectedUser = null;
        if (mSelectedPosition != null) {
            currentSelectedUser = mUsersList.get(mSelectedPosition);
        }

        SortUtil.sortAZ(mUsersList);

        if (currentSelectedUser != null) {
            mSelectedPosition = SortUtil.searchNewPosition(currentSelectedUser, mUsersList);
        }
    }

    /**
     *  Sort the list alphabetically in reverse order and get the new user position.
     */
    void sortRecyclerViewZA() {
        User currentSelectedUser = null;
        if (mSelectedPosition != null) {
            currentSelectedUser = mUsersList.get(mSelectedPosition);
        }

        SortUtil.sortZA(mUsersList);

        if (currentSelectedUser != null) {
            mSelectedPosition = SortUtil.searchNewPosition(currentSelectedUser, mUsersList);
        }

    }

    List<User> getUsersList() {
        return mUsersList;
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvUsername)
        TextView tvName;
        @BindView(R.id.tvEmail)
        TextView tvEmail;

        ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        void onClick() {
            mClickHandler.click(getAdapterPosition(), mUsersList.get(getAdapterPosition()));

            if (mSaveClickPosition) {
                // Updating old as well as new positions
                if (mSelectedPosition != null) {
                    notifyItemChanged(mSelectedPosition);
                }
                mSelectedPosition = getAdapterPosition();
                notifyItemChanged(mSelectedPosition);
            }

        }
    }

    interface AdapterOnClickHandler {
        void click(int position, User user);
    }

}
