package com.fearaujo.orionhealthtest.application;

import android.app.Application;

import com.fearaujo.data.injection.ApplicationComponent;
import com.fearaujo.data.injection.ContextModule;
import com.fearaujo.data.injection.DaggerApplicationComponent;
import com.fearaujo.orionhealthtest.home.HomeComponent;

/**
 * Created by felipearaujo on 08/01/17.
 */

public class AppApplication extends Application {

    private ApplicationComponent applicationComponent;

    private HomeComponent mHomeComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder().
                contextModule(new ContextModule(getApplicationContext()))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public HomeComponent getHomeComponent() {
        return mHomeComponent;
    }

    public void setHomeComponent(HomeComponent mHomeComponent) {
        this.mHomeComponent = mHomeComponent;
    }
}
