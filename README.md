Hello,
Thank you very much for the opportunity, here are some considerations about the project.

* Frameworks used:
Dagger 2 - Used to aid in the injection of dependencies.
Butterknife - Used to inject UI components.
Retrofit 2 - Used to perform network requests.
Gson - Used for mapping JSONs.
Retrolambda - To use lambdas.

* App implemented in MVP (Model View Presenter).

* The project was divided into 2 modules (app and data);
The DATA module was responsible for:
- Mapping the models.
- Implementation of communication with retrofit.
- Implementation of DI modules for Retrofit.
- Some utilitarian classes.

The APP module was responsible for:
- Implementation of Activities and fragments.
- Presenters
- Layouts
- Internationalization (English and Portuguese)
- Dagger Component
- Implementation of DI modules for UI.

* Dagger 2 - I like to use Dagger to inject dependencies. I believe that the framework provides many benefits to the project, making it easier to handle multiple dependencies and tests.
* Retrofit - I believe it's currently the best communication framework for Android, its ease and integration with other frameworks makes it very powerful (ex: RxJava).
* Constraint Layout - Although still in beta, I really enjoy using the new Constraint Layout, because it provides incredible agility in the construction of layouts and is sure to be a tool widely used by all Android developers.
* MVP - I believe to be one of the best presentation standards for Android, for simplicity, great layering and ease of testing.

That was only a short summary of the test, I hope it was clear.
Any doubts or questions do not hesitate to contact me.

Again, thank you very much for the opportunity.

Kind regards.

Felipe Araujo