package com.fearaujo.data.injection;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by felipearaujo on 08/01/17.
 *
 * Dagger Module responsible to provide the application context.
 */
@Module
public class ContextModule {

    private final Context mContext;

    public ContextModule(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    public Context providesApplicationContext() {
        return mContext;
    }

}
