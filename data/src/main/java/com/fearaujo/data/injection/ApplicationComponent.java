package com.fearaujo.data.injection;

import com.fearaujo.data.service.IUserAPI;

import dagger.Component;

/**
 * Created by felipearaujo on 08/01/17.
 */
@ApplicationScope
@Component(modules = {NetworkModule.class, ServiceModule.class, ContextModule.class})
public interface ApplicationComponent {

    IUserAPI getUserService();

}
