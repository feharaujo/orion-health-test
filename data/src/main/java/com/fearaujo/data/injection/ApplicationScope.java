package com.fearaujo.data.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by felipearaujo on 08/01/17.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ApplicationScope {
}
