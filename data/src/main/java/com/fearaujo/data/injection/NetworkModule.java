package com.fearaujo.data.injection;

import android.content.Context;

import com.fearaujo.data.util.NetworkUtil;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by felipearaujo on 08/01/17.
 *
 * Dagger module responsible for provide an @{@link OkHttpClient} for Retrofit
 */
@Module
public class NetworkModule {

    @ApplicationScope
    @Provides
    public OkHttpClient providesOkHttpClient(Cache cache, Interceptor interceptor) {

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.networkInterceptors().add(interceptor);
        okHttpClient.writeTimeout(60L, TimeUnit.SECONDS);
        okHttpClient.readTimeout(60L, TimeUnit.SECONDS);
        okHttpClient.connectTimeout(60L, TimeUnit.SECONDS);
        okHttpClient.cache(cache);
        return okHttpClient.build();

    }

    @ApplicationScope
    @Provides
    public Cache provideCache(File cacheFile) {
        // 10 MB of cache
        return new Cache(cacheFile, 10 * 1024 * 1024);
    }

    @ApplicationScope
    @Provides
    public File providesCacheFile(Context context) {
        return new File(context.getCacheDir(), "http-cache");
    }

    @ApplicationScope
    @Provides
    public Interceptor providesCacheInterceptor(Context context) {
        return chain -> {
            Response originalResponse = chain.proceed(chain.request());
            if (NetworkUtil.isNetworkConnected(context)) {
                int maxAge = 60; // read from cache for 1 minute
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            } else {
                int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .build();
            }
        };
    }

}
