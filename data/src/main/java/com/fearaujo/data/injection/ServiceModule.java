package com.fearaujo.data.injection;

import com.fearaujo.data.service.IUserAPI;
import com.fearaujo.data.util.Urls;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by felipearaujo on 08/01/17.
 *
 * Dagger module responsible for provide an {@link IUserAPI} for retrofit requests.
 */
@Module(includes = NetworkModule.class)
public class ServiceModule {

    @Provides
    @ApplicationScope
    public IUserAPI providesUserAPI(Retrofit retrofit) {
        return retrofit.create(IUserAPI.class);
    }

    @Provides
    @ApplicationScope
    public Retrofit providesRetrofit(Retrofit.Builder builder) {
        return builder
                .baseUrl(Urls.BASE_URL)
                .build();
    }

    @ApplicationScope
    @Provides
    public Retrofit.Builder providesRetrofitBuilder(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory);
    }

    @ApplicationScope
    @Provides
    public GsonConverterFactory providesGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

}
