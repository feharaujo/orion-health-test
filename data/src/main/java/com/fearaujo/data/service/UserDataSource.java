package com.fearaujo.data.service;

import com.fearaujo.data.model.User;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by felipearaujo on 08/01/17.
 */

public class UserDataSource {

    private final IUserAPI mUserService;

    @Inject
    public UserDataSource(IUserAPI userService) {
        this.mUserService = userService;
    }

    /**
     * Search for all users
     *
     * @param callback Callback response
     */
    public void searchUsers(Callback<List<User>> callback) {
        Call<List<User>> call = mUserService.fetchUsers();
        call.enqueue(callback);
    }

}
