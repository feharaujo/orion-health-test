package com.fearaujo.data.service;

import com.fearaujo.data.model.User;
import com.fearaujo.data.util.Urls;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by felipearaujo on 08/01/17.
 */
public interface IUserAPI {

    @GET(Urls.ENDPOINT)
    Call<List<User>> fetchUsers();

}
