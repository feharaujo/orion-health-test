package com.fearaujo.data.util;

import com.fearaujo.data.model.User;

import java.util.Collections;
import java.util.List;

/**
 * Created by felipearaujo on 09/01/17.
 * <p>
 * Util methods to sort the Users.
 */
public class SortUtil {

    /**
     * Sort list alphabetically (A-Z)
     *
     * @param users List of users
     * @return Ordered list
     */
    public static List<User> sortAZ(List<User> users) {
        Collections.sort(users, (user, t1) -> user.getName().compareTo(t1.getName()));
        return users;
    }

    /**
     * Sort the list alphabetically in reverse order
     *
     * @param users List of users
     * @return Ordered list
     */
    public static List<User> sortZA(List<User> users) {
        Collections.sort(users, (user, t1) -> t1.getName().compareTo(user.getName()));
        return users;
    }

    /**
     * This method returns the position that the currently selected user is after sorting.
     *
     * @param user       Current selected user
     * @param sortedList List of users sorted
     * @return Position of the user
     */
    public static int searchNewPosition(User user, List<User> sortedList) {
        int id = user.getId();
        int newPosition = -1;

        for (int i = 0; i < sortedList.size(); i++) {
            if (sortedList.get(i).getId() == id) {
                newPosition = i;
                break;
            }
        }

        return newPosition;
    }

}
