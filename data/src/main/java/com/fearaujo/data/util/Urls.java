package com.fearaujo.data.util;

/**
 * Created by felipearaujo on 08/01/17.
 *
 * Url utils.
 */
public class Urls {

    /**
     * Base url
     */
    public static final String BASE_URL = "http://jsonplaceholder.typicode.com";

    /**
     * Endpoint
     */
    public static final String ENDPOINT = "/users";

}
