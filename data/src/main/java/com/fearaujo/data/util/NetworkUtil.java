package com.fearaujo.data.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by felipearaujo on 09/01/17.
 */

public class NetworkUtil {

    /**
     * Check the device connection
     *
     * @param context Application context
     * @return Network status
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
